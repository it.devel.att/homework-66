class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_back(fallback_location: root_path, :alert => exception.message)
  end

  def access_denied(exception)
    flash[:danger] = exception.message
    redirect_back(fallback_location: root_path)
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :login, :email])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:login])
  end
end
