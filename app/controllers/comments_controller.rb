class CommentsController < ApplicationController
  before_action :set_comment, only: [:destroy]
  before_action :set_user, :set_post, only: [:create]
  load_and_authorize_resource :only => [:destroy]

  def create
    @comment = Comment.new(comment_params)
    @comment.post = @post
    @comment.user = current_user

    if @comment.save
      flash[:success] = "You create comment!"
      redirect_to user_post_path(@user, @post)
    else
      flash[:danger] = "#{@comment.errors.full_messages.join(", ")}"
      redirect_to user_post_path(@user, @post)
    end
  end

  def destroy
    @comment.destroy
    flash[:success] = "You delete comment!"
    redirect_back(fallback_location: root_path)
  end


  private
  def set_user
    @user = User.find(params[:user_id])
  end

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:text)
  end
end
