class BanList < ApplicationRecord
  belongs_to :user

  validate :validate_end_at_user_ban


  def end_at_less_than_current_time
    min_time = Time.zone.now + 6.hours
    if end_at && end_at.utc < min_time.utc
      true
    else
      false
    end
  end

  def validate_end_at_user_ban
    if end_at.nil?
      errors.add(:end_at, " can't be nil!")
    else
      errors.add(:end_at, " of ban time less than current time") if end_at_less_than_current_time
    end
  end
end
