class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable

  has_many :posts, :dependent => :destroy
  has_many :comments
  has_many :likes
  has_many :ban_lists


  has_and_belongs_to_many :followers, :class_name => "User", :join_table => 'followers_following', foreign_key: :follower_id, :association_foreign_key => :following_id
  has_and_belongs_to_many :followings, :class_name => "User", :join_table => 'followers_following', foreign_key: :following_id, :association_foreign_key => :follower_id


  validates :login, presence: true, uniqueness: true, length: { in: 5..20 }
  validates :name, presence: true, length: { in: 2..20 }
  validates :status, presence: true, inclusion: { in: %w(active ban) }


  def change_status_if_ban_time_end
    if status == "ban"
      current_time = Time.new
      # binding.pry
      if ban_lists.order(created_at: 'desc').first && ban_lists.order(created_at: 'desc').first.end_at < current_time.utc
        update_attribute(:status, "active")
        return true
      end
    end
  end

  def account_active?
    change_status_if_ban_time_end
    return true if status == "active"
    return false if status == "ban"
  end

  def active_for_authentication?
    super && account_active?
  end

  def inactive_message
    if status == "ban"
      "You was banned! End of ban will be in #{ban_lists.last.end_at.localtime}"
    end
  end
end
