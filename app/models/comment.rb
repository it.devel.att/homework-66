class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  scope :open_status, -> { where(status: "open") }

  validates :status, presence: true, inclusion: { in: %w(open hide), message: "must include open or hide" }

  validates :text, presence: true, length: { in: 3..150 }
end
