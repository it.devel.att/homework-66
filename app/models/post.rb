class Post < ApplicationRecord
  belongs_to :user
  has_many_attached :images
  has_many :comments, :dependent => :destroy
  has_many :likes, :dependent => :destroy
  has_many :users, through: :likes

  scope :open_status, -> { where(status: "open") }

  validates :status, presence: true, inclusion: { in: %w(open hide), message: "must include open or hide" }

  validates :images, attached: true, content_type: ['image/png', 'image/jpg', 'image/jpeg']
  validates :like_qty, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
