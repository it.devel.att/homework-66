# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Post, :status => "open"
    can :read, Comment
    can :read, User
    if user.present?
      can :manage, Post, user_id: user.id
      can :create, Comment
      can :destroy, Comment, user_id: user.id
      if user.admin?
        can :read, :all
        can :post_block, Post, :status => "open"
        can :post_unblock, Post, :status => "hide"
        can :comment_block, Comment, :status => "open"
        can :comment_unblock, Comment, :status => "hide"
        can :user_ban, User, admin: false, status: "active"
        can :user_unban, User, admin: false, status: "ban"
        can :create, BanList
      end
    end
  end
end
