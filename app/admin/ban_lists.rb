ActiveAdmin.register BanList do
  config.filters = false

  permit_params :user_id, :end_at

  controller do
    def create
      ban_record = build_resource
      save_resource(ban_record)
      ban_user = User.find(params[:ban_list][:user_id])
      ban_user.assign_attributes(:status => "ban")
      if ban_user.admin?
        ban_user.assign_attributes(:status => "active")
      end
      if ban_user.save && ban_record.valid?
        redirect_to admin_ban_lists_path
      else
        render :new
      end
    end
  end

  index do
    id_column
    column :user_id do |ban|
     link_to ban.user.login, admin_user_path(ban.user)
   end
   column :created_at
   column :end_at
   column "Status" do |ban|
    ban.user.status
  end
  actions
end

show do
  attributes_table do
    row :user_id do |ban|
      ban.user.login
    end
    row :created_at
    row :end_at
  end

end


form do |f|
  f.semantic_errors *f.object.errors.keys
  f.input :user_id, :label => "User", :as => :select, :collection => User.all.map{ |u| ["#{u.login}", u.id] }, selected: 1, include_blank: false
  f.input :end_at
  f.actions
end



end
