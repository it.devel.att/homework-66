ActiveAdmin.register User do
  config.filters = false


  controller do
    def ban_user
      user = resource
      user.status = "ban"
      user.save
      user.ban_lists.create(end_at: Time.new + 1.day)

      redirect_back(fallback_location: root_path, alert: "You banned user")
    end

    def unban_user
      user = resource
      user.status = "active"
      user.save
      # ban_list = user.ban_lists.last
      # ban_list.destroy if ban_list

      redirect_back(fallback_location: root_path, alert: "You Unbanned user")
    end

  end

  member_action :user_ban, method: :put do
    ban_user
  end

  member_action :user_unban, method: :put do
    unban_user
  end


  index do
    column :login do |user|
      link_to user.login, admin_user_path(user)
    end
    column :name
    column :email
    column :admin
    column :status
    column "End of Ban", :ban_lists do |user|
      unless user.ban_lists.empty?
        user.ban_lists.last.end_at if user.status == "ban"
      end
    end
    actions do |user|
      item 'Ban user (1 day)', user_ban_admin_user_path(user), method: :put if user.status == "active" && !user.admin?
      item 'Lucky Unban', user_unban_admin_user_path(user), method: :put if user.status == "ban" && !user.admin?
    end
  end

  show do
    attributes_table do
      row :login
      row :name
      row :email
      row :admin
      row :status
    end
  end
end
