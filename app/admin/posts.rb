ActiveAdmin.register Post do
  actions :index, :show
  config.filters = false

  controller do

    def block_post
      post = resource
      post.status = "hide"
      if post.save
       redirect_back(fallback_location: root_path, alert: "You are succefull hide post")
     else
      redirect_back(fallback_location: root_path, alert: "Wrong! #{post.errors.full_messages.first}")
    end
  end

  def unblock_post
    post = resource
    post.status = "open"
    if post.save
      redirect_back(fallback_location: root_path, alert: "You are unblock post")
    else
      redirect_back(fallback_location: root_path, alert: "Wrong! #{post.status.full_messages.first}")
    end
  end

end

member_action :post_block, method: :put do
  block_post
end

member_action :post_unblock, method: :put do
  unblock_post
end

index do
  id_column
  column "Images" do |post|
    post.images.each do |img|
      span do
        image_tag img.variant(combine_options: {resize: "100"})
      end
    end
    ""
  end
  column "Descriptions", :desc
  column "Likes", :like_qty
  column "Status",:status
  column "Author", :user_id do |post|
    link_to post.user.login, admin_user_path(post.user)
  end
  actions do |post|
    item "Hide", post_block_admin_post_path(post), method: :put if post.status == "open"
    item "Unblock", post_unblock_admin_post_path(post), method: :put if post.status == "hide"
  end
end

show do
  attributes_table do
    row "Images" do |post|
      post.images.each do |img|
        span do
          image_tag img.variant(combine_options: {resize: "200"})
        end
      end
      ""
    end
    row :desc
    row :like_qty
  end
end

end
