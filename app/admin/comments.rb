ActiveAdmin.register Comment, as: "PostComment" do
  actions :index, :show
  config.filters = false

  controller do

    def block_comment
      comment = resource
      comment.status = "hide"
      if comment.save
        redirect_back(fallback_location: root_path, alert: "You are hide comment")
      else
        redirect_back(fallback_location: root_path, alert: "Wrong params! #{comment.errors.full_messages.first}")
      end
    end

    def unblock_comment
      comment = resource
      comment.status = "open"
      if comment.save
        redirect_back(fallback_location: root_path, alert: "You are unblock comment")
      else
        redirect_back(fallback_location: root_path, alert: "Wrong params! #{comment.errors.full_messages.first}")
      end
    end

  end

  member_action :comment_block, method: :put do
    block_comment
  end

  member_action :comment_unblock, method: :put do
    unblock_comment
  end


  index do
    id_column
    column :text
    column :post_id do |comment|
      comment.post
    end
    column :user_id do |comment|
      link_to comment.user.login, admin_user_path(comment.user)
    end
    column :status
    actions do |comment|
      item "Hide comment", comment_block_admin_post_comment_path(comment), method: :put if comment.status == "open"
      item "Unblock comment", comment_unblock_admin_post_comment_path(comment), method: :put if comment.status == "hide"
    end
  end
end
