module PostsHelper

  def check_for_subscribe(post)
    true if user_signed_in? && !current_user.followings.include?(post.user) && current_user != post.user
  end

  def check_for_unsubscribe(post)
    true if user_signed_in? && current_user.followings.include?(post.user)
  end


end
