$( document ).on('turbolinks:load', function() {
  $('.slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
  });
  if ($('#flashMessages').length){
    $('#flashMessages').fadeOut(5000);
  };
});
