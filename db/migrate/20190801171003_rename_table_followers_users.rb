class RenameTableFollowersUsers < ActiveRecord::Migration[5.2]
  def change
    rename_table :followers_users, :followers_following
  end
end
