class RemoveQtyColumnFromLikes < ActiveRecord::Migration[5.2]
  def change
    remove_column :likes, :qty
  end
end
