class AddStatusToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :status, :string, default: "open"
  end
end
