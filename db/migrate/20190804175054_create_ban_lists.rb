class CreateBanLists < ActiveRecord::Migration[5.2]
  def change
    create_table :ban_lists do |t|
      t.references :user, foreign_key: true
      t.datetime :end_at

      t.timestamps
    end
  end
end
