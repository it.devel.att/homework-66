class AddLikesQtyToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :like_qty, :integer, default: 0
  end
end
