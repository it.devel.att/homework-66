require 'test_helper'

class LikesControllerTest < ActionDispatch::IntegrationTest
  test "should get up" do
    get likes_up_url
    assert_response :success
  end

  test "should get down" do
    get likes_down_url
    assert_response :success
  end

end
